<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of App
 *
 * @author emman
 */
class App {
    //put your code here
    
    
    private $_controller = 'loginController';
    private $_method = 'index';
    private $_params = [];
    
    
    public function __construct() {
        
        $url = $this->parse_url();
        session_start();
        
        if(file_exists(ROOT.'/controllers/'.$url[0].'.php')){
            
            $this->_controller = $url[0];
            unset($url[0]);
        }
        
        require_once ROOT.'controllers/'.$this->_controller.'.php';
        $this->_controller = new $this->_controller;
        
        if(isset($url[1])){
            
            if(method_exists($this->_controller,$url[1])){
                $this->_method = $url[1];
                
                unset($url[1]);
                
            }
        }
        
        
        $this->_params = $url ? array_values($url) : [];
        
        call_user_func_array([$this->_controller,$this->_method],[$this->_params]);
        
    }
    
    public function parse_url(){
    
           if(isset($_GET['url'])){
               $url = explode('/', filter_var(rtrim($_GET['url'],'/'),FILTER_SANITIZE_URL));
              
               return $url;
           }
    }
}
