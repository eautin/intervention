<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Controller
 *
 * @author emman
 */
class Controller {
    //put your code here
    
    public function view($view, $data = null){
       require_once ROOT.'views/'.$view.'.php';
    }
    
    public function model($model){
        require_once ROOT.'models/'.$model.'.php';
        return new $model();
    }
}
