<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ajaxController
 *
 * @author emman
 */

class ajaxController{
    
    private $_model;
    
    public function __construct() {
        
        // pour ajax, on charge toutes les ressources dans le constructeur  
        require_once '../core/Model.php';
        require_once '../models/ajaxModel.php';
        require_once '../library/tcpdf/tcpdf.php';
        $this->model = new ajaxModel();
    }
    
    public function checkDate($date) {
        
        if($this->model->checkdate($date)){
            return true;
        }else{
            return false;
        }
    }
    
    
    public function get_tech_date_hours($date){
        
        $date = $this->model->get_tech_date_hours($date);
        
        return $date;
    }
    
    
    public function checkHours($date){
         if($this->model->checkDateHours($date)){
            return true;
        }else{
            return false;
        }
    }
    
    
    public function getTechData(){
        
         
         $tech_data = $this->model->getTechData();
         return $tech_data;
    }
    
    
    public function updateIntervention(){
        
    }
    
    
    public function insertInterventions($interventions, $dayInter){
        
        
      if($this->model->insertInterventions($interventions, $dayInter)){
            return true;
        }
        
        // now generation pdf 
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        
        var_dump($pdf);
    }
    
    
     public function updateInterventions($interventions, $dayInter){
        
     
        
        if($this->model->updateInterventions($interventions, $dayInter)){
            return true;
        }
        
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    }
    
   
    public function getDayData($day) {
        
        $dayDatas = $this->model->getDayData($day);
        
        return $dayDatas; 
    }
    
    
    public function getPlageInterventions($day_start, $day_end){
        
        $interventions = $this->model->get_plage_interventions($day_start, $day_end);
        return $interventions;
    }
    
    
    
    public function get_cumul_interventions($dayStart, $dayEnd){
        
        $cumuls['interventions'] = $this->model->get_cumul_interventions($dayStart, $dayEnd);
        $cumuls['techniciens'] = $this->model->get_techniciens();
        return $cumuls;
    }
    
    
    public function get_prestas(){
        
        $prestas = $this->model->get_prestas();
        
        return $prestas;
    }
    
      public function get_techniciens(){
        
        $techs = $this->model->get_techniciens();
        
        return $techs;
    }
    
    
    public function add_presta($presta_name, $presta_msg){
        
        
        $test = $this->model->add_presta($presta_name, $presta_msg); 
        
        return $test;
    }
    
     public function add_tech($tech_data){
        
        
        $insert_tech = $this->model->add_tech($tech_data); 
        
        return $insert_tech;
    }
    
    public function get_vols( ){
        
        $vols = $this->model->get_vols();
        
        return $vols;
    }
    
    
    public function update_vol($vol){
        
        $update = $this->model->update_vol($vol);
        
        return $update;
    }
    
    
    public function insertHoraires($date, $isFerie, $isDimanche, $horaires){
        
        $insert = $this->model->insert_horaires($date, $isFerie, $isDimanche, $horaires);
        
        return $insert;
    }
    
   
}
