<script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
crossorigin="anonymous"></script>
<script src="<?php echo CONTROLLER_PATH; ?>views/assets/js/bootstrap.min.js"></script>
<script src="<?php echo CONTROLLER_PATH; ?>views/assets/calendar/js/moment.js"></script>
<script src="<?php echo CONTROLLER_PATH; ?>views/assets/calendar/js/fullcalendar.js"></script>
<script src="<?php echo CONTROLLER_PATH; ?>views/assets/calendar/locale/fr.js"></script>
<script src="<?php echo CONTROLLER_PATH; ?>views/assets/js/config.js"></script>
<script src="<?php echo CONTROLLER_PATH; ?>views/assets/js/app_vols.js"></script>
<footer>
    <p class="mention">2018 - rapport.tsip-telesurveillance.fr</p>
</footer>
</body>

</html>
