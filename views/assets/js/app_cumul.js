
$(function(){


                    
  $('#getDates').click(function(){
      
      
      var dayStart = $('#startDate').val();
      var dayEnd = $('#endDate').val(); 
      getResults(dayStart, dayEnd); 
      
      
  });
                    
     $('#calendar').fullCalendar({
                // put your options and callbacks here
                locale: 'fr',
                showNonCurrentDates: false,
                selectable: true,
                dayClick: function (date) {
                    date = date.format( );
                    
                    console.log(date);
                },
                select: function(dayStart, dayEnd){
                    dayStart = dayStart.format( );
                    dayEnd = dayEnd.subtract('1','days');
                    dayEnd = dayEnd.format();
                    if(dayStart === dayEnd){
                        
                        $('#modalInfo').modal( );

                    }else{
                        
                        getResults(dayStart, dayEnd); 
                          
                        
                    }

                },
                unselect: function(date){
                    
                }  
    });
     
    
    $('#closePopin').click(function(){
        $('#popinDayStart').empty( );
        $('#popinDayEnd').empty( );
        $('#formInjector').empty();
    });
    
    
    function getResults(dayStart, dayEnd){
        
        $.ajax({
                        type: "POST",
                        data: {
                            getInterventions: "récupère les interventions entre deux dates",
                            dayStart: dayStart,
                            dayEnd: dayEnd
                        },
                        url: urlAjax,
                        success: function(response) {
                            
                             $('#popinDayStart').empty( );
                             $('#popinDayEnd').empty( );
                             $('#formInjector').empty();
                             
                            var response = JSON.parse(response);
                            
                            
                            console.log(response);
                            var typeInter = ["ronde_tsip", "ronde_autre", "gardiennage", "vol_confirme", "intrusion_vol", "incendie", "agression", "technique", "absence", "mes_anomalie", "mos_anomalie", "coupure_edf", "coupure_ptt", "autoprotection", "ouverture_fermeture"];
                    
                                // pour chaque tech
                                $.each(response.techniciens, function(cle, tech){
                                    
                                    var row = "<tr>";
                                    var name = tech.nom;
                                    var prename = tech.prenom;
  
                                    console.log(tech);
                                    
                                    row += "<td>"+name+" "+prename+"</td>";
                                    
                                    var store = [
                                        '<td><input type="number" data-id="" name="ronde_tsip" value="0" class="form-control" disabled min="0"></td>',
                                        '<td><input type="number" data-id="" name="ronde_autre" value="0" class="form-control" disabled min="0"></td>',
                                        '<td><input type="number" data-id="" name="gardiennage" value="0" class="form-control" disabled min="0"></td>',
                                        '<td><input type="number" data-id="" name="vol_confirme" value="0" class="form-control" disabled min="0"></td>',
                                        '<td><input type="number" data-id="" name="intrusion_vol" value="0" class="form-control" disabled min="0"></td>',
                                        '<td><input type="number" data-id="" name="incendie" value="0" class="form-control" disabled min="0"></td>',
                                        '<td><input type="number" data-id="" name="agression" value="0" class="form-control" disabled min="0"></td>',
                                        '<td><input type="number" data-id="" name="technique" value="0" class="form-control" disabled min="0"></td>',
                                        '<td><input type="number" data-id="" name="absence" value="0" class="form-control" disabled min="0"></td>',
                                        '<td><input type="number" data-id="" name="mes_anomalie" value="0" class="form-control" disabled min="0"></td>',
                                        '<td><input type="number" data-id="" name="mos_anomalie" value="0" class="form-control" disabled min="0"></td>',
                                        '<td><input type="number" data-id="" name="coupure_edf" value="0" class="form-control" disabled min="0"></td>',
                                        '<td><input type="number" data-id="" name="coupure_ptt" value="0" class="form-control" disabled min="0"></td>',
                                        '<td><input type="number" data-id="" name="autoprotection" value="0" class="form-control" disabled min="0"></td>',
                                        '<td><input type="number" data-id="" name="ouverture_fermeture" value="0" class="form-control" disabled min="0"></td>'
                                        ];
                                        
                                        // chaque type d'inter 
                                        $.each(typeInter, function(cle, val){
                                                
                                                   $.each(response.interventions, function(key, inter){
                                                       
                                                       console.log(inter);
                                                       if(val === inter.type_intervention && tech.id === inter.id_technicien){
                                                           store[cle] = '<td><input type="number" data-id="'+inter.id_technicien+'" name="'+val+'" value="'+inter.nb_intervention+'" class="form-control" disabled min="0"></td>';
                                                       }
                                                   });
                                        });
                                        
                                        row += store[0]+store[1]+store[2]+store[3]+store[4]+store[5]+store[6]+store[7]+store[8]+store[9]+store[10]+store[11]+store[12]+store[13]+store[14];
                                        row += '<tr>';
                                        $('#formInjector').append(row);
                                 
                                });
                                
   
                            
                            $('#modalCumul').modal( );
                            $('#popinDayStart').html(moment(dayStart).format('dddd Do MMMM YYYY'));
                            $('#popinDayEnd').html(moment(dayEnd).format('dddd Do MMMM YYYY'));
                            
                        },
                        error: function(err){
                            
                            console.log(err);
                        }

                        });  
        
    }
    
    });
    