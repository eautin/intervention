  $(function() {

  var currDate = {
      day: ""
  };

  if ($('#calendar').length != 0) {

      $('#calendar').fullCalendar({
          // put your options and callbacks here
          locale: 'fr',
          showNonCurrentDates: false,
          dayClick: function(date) {

              var date = date.format();
              currDate.day = date;
              $('#formInjector').empty();
              $('#formInjectorEdit').empty();
              $('#modal-resp-edit-success').hide();
              $('#modal-resp-edit-success').empty();
              $('#modal-resp-saisie-success').hide();
              $('#modal-resp-saisie-success').empty();
              $('#modal-resp-saisie-success').empty();
              $('#saveEditDate').show();
              $('#saveNewDate').show();
              $('#dynamicDayEdit').attr('data-day', '');
              $('#dynamicDay').attr('data-day', '');
              $('#dynamicDayEdit').attr('data-day', date);
              $('#dynamicDay').attr('data-day', date);

              $.ajax({
                  type: "POST",
                  data: {
                      check_if_edit_hours: "verifier si heures déjà saisie pour la date.",
                      date: date
                  },
                  url: urlAjax,
                  success: function(data) {
                      
                      try {
                          var response = JSON.parse(data);
                      } catch (e) {
                          console.error(e)
                          console.error('JSON reçue: ', data)
                      }

                      var dateFR = moment(response.date).format('dddd Do MMMM YYYY');
                      
                   
                      // date deja saisie
                      if (response.statut === true) {

                        
                        
                      } else {

                          $('#dynamicDay').html(dateFR);
                          $('#modal-resp-saisie-success').empty();
                          $('#formInjector').empty();
                          $('#saveNewDate').show();
                          var formHTML = '';

                          $.each(response.tech_data, function(key, value) {

                              formHTML += '<tr>';
                              formHTML += '<td>' + value.nom + " " + value.prenom + '</td>';
                              formHTML += '<td><input type="time" data-id="' + value.id + '" name="debut_matin" class="form-control" value="0" min="0"></td>';
                              formHTML += '<td><input type="time" data-id="' + value.id + '" name="fin_matin" class="form-control" value="0" min="0"></td>';
                              formHTML += '<td><input type="time" data-id="' + value.id + '" name="debut_aprem" class="form-control" value="0" min="0"></td>';
                              formHTML += '<td><input type="time" data-id="' + value.id + '" name="fin" class="form-control" value="0" min="0"></td>';
                              formHTML += '</tr>';
                          });
                          
                          formHTML += '<tr><td>Jour férié :</td><td><input type="checkbox" id="is_ferie" name="is_ferie" class="form-control"/></td></tr>';
                          
                          $('#formInjector').append(formHTML);
                          $('#modalSaisie').modal();
                      }
                  }
              });
          }
      });
  }


  
  $('#saveNewHoraire').click(function(){ 
     
     var currDay = currDate.day;
     var isFerie; 
     var isDimanche = (moment(currDate.day).day() === 0) ? true : false;
     
    if($('#is_ferie').is(':checked')){
           isFerie = true;
     }else{
         isFerie = false;
     }
     
     console.log(isFerie);
     
     $inputs = $('[type=time]');
      
       var horaires = $inputs.map(function () {
      
                var val = $(this).val();
                return {

                    techid: $(this).data('id'),
                    name: $(this).attr('name'),
                    value: $(this).val()
                }

            }).get();
            
             
            var dataTech = {};
            var lastId = "";
            
            
            // build the tech object 
            horaires.forEach(function(horaire, value){
                
                if(lastId != horaire.techid){
                    
                    dataTech[horaire.techid] = {
                        
                        debut_matin: "",
                        fin_matin: "",
                        debut_aprem:"",
                        fin: "",
                        ferie: isFerie
                      
                    }
                }
 
                lastId = horaire.techid;
                
            });
            
            
            //refill the tech object
            horaires.forEach(function(horaire, value){
                
                 var currId = horaire.techid;
                 var currProp = horaire.name
                 var currVal = horaire.value;
                 
                 dataTech[currId][currProp] = currVal;
                
            });
            
            
 
            var data = {
                      save_horaires: "save horaires day in bdd",
                      horaires: dataTech,
                      date: currDay,
                      is_ferie: isFerie,
                      is_dimanche: isDimanche
                  };
       
    
         $.ajax({
                  type: "POST",
                  data: data,
                  url: urlAjax,
                  success: function(data) {
                      
                      console.log(data);

                      $success = '<div class="alert alert-success"><strong>Mise à jour des horaires pour le "'+data+'"</strong></div>';
                      $('#modal-resp-edit-success').append($success);
                      $('#modal-resp-edit-success').show();
                      $('#saveEditDate').hide();
                      $('#dynamicDayEdit').attr('data-day', '');
                      $('#dynamicDay').attr('data-day', '');


                  }


              });
   
  });
  
  
  });








  