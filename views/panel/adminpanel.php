<?php if (isset($_SESSION) && isset($_SESSION['role'])) { ?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h2>Cumul des interventions</h2>
            <p>Séléctionnez la période pour obtenir le nombre d'intervention réalisées par technicien.</p>
        </div>
    </div>
    <div class="row">
                <div class="col-lg-6 col-sm-12">
                     <label for="dayStart">Jour début</label>
                <input type="date" name="dayStart" class="form-control" id="startDate">
                </div>
                <div class="col-lg-6 col-sm-12">
                    <label for="dayEnd">Jour fin</label>
                <input type="date" name="dayEnd" class="form-control" id="endDate">
                </div>
                <div class="col-md-12">
                    <button type="button" style="margin-top:1rem;" class="btn btn-primary" id="getDates">Obtenir les interventions</button>
                </div>
    </div>
    <div class="row">
         <div class="col-lg-12">
        
            <div id="calendar">
                
            </div>
        </div>
    </div>
    </div>  
</div>

<?php }else{ exit; }?>
