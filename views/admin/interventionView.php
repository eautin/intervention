
<?php require_once ROOT.'views/structure/header.php'; 


  foreach($data as $row) {
                          
    $id = $row['id'] ;
    $nom = $row['nom'];
    $prenom = $row['prenom'];
    
                       
  }  ?>

  <div class="container">
      <div class="row">
          <div class="col-lg-6 offset-3">
            <form action="<?php echo CONTROLLER_PATH;?>adminController/saveIntervention" id="selectTech" class="form-signin" method="POST">
                <div class="form-group">
                    <h2>Saisir les interventions du jour pour <?php echo $nom." ".$prenom; ?></h2>
                    <div class="form-group">
                        <label for="ronde_tsip">Date des interventions</label>
                        <input type="date" name="date_intervention" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="ronde_tsip">Ronde TSIP</label>
                        <input type="number" name="ronde_tsip" class="form-control" value="0" min="0">
                    </div>
                     <div class="form-group">
                        <label for="poste_fixe">Poste fixe</label>
                        <input type="number" name="poste_fixe" class="form-control" value="0" min="0">
                        </div>
                  
                    <div class="form-group">
                        <label for="ronde_tsip">D.I</label>
                        <input type="number" name="DI" class="form-control" value="0" min="0">
                    </div>
                    <div class="form-group">
                        <label for="ronde_tsip">Intrusion & Vol</label>
                        <input type="number" name="intrusion_vol" class="form-control" value="0" min="0">
                        </div>
                   <div class="form-group">
                        <label for="ronde_tsip">Erreur client</label>
                        <input type="number" name="erreur_client" class="form-control" value="0" min="0">
                        </div>
                   <div class="form-group">
                        <label for="ronde_tsip">Incendie</label>
                        <input type="number" name="incendie" class="form-control" value="0" min="0">
                        </div>
                   <div class="form-group">
                        <label for="ronde_tsip">Agression</label>
                        <input type="number" name="agression" class="form-control" value="0" min="0">
                        </div>
                   <div class="form-group">
                        <label for="ronde_tsip">Test cyclique</label>
                        <input type="number" name="test_cyclique" class="form-control" value="0" min="0">
                    </div>
                   </div>
                <div class="form-group">
                        <label for="ronde_tsip">A.P</label>
                        <input type="number" name="a_p" class="form-control" value="0" min="0">
                        </div>
                   <div class="form-group">
                        <label for="intervention_autre">Intervention Autre</label>
                        <input type="number" name="intervention_autre" class="form-control" value="0" min="0">
                    </div>
                        <input type="hidden" name="id_tech" value="<?php echo $id; ?>">
                
                <div class="form-group">       
                    <input type="submit"  name="saveIntervention" class="btn btn-lg btn-success btn-block" value="Enregister"/>
                </div>
            </form>
          </div>
      </div>
  </div>

<?php require_once ROOT.'views/structure/footer.php'; ?>