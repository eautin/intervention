
<?php require_once ROOT.'views/structure/header.php'; ?>

  <div class="container">
      <div class="row">
          <div class="col-lg-6 offset-lg-3">
              <div class="alert alert-success">
                  Intervention enregistrée avec succès !
              </div>
              <a class="btn btn-lg btn-success btn-block" href="<?php echo CONTROLLER_PATH; ?>adminController/selectTech">Saisir le rapport pour un autre collaborateur</a>
          </div>
      </div>
  </div>

<?php require_once ROOT.'views/structure/footer.php'; ?>