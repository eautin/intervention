<?php

 

class adminModel extends Model {
    
    
        public function getTechList( ) {
        $bdd = parent::getBdd();
        $req = $bdd->prepare("SELECT id, nom, prenom FROM technicien");
        $req->execute();
        $techs = $req->fetchAll(PDO::FETCH_ASSOC);
        return $techs;
        
        }
        
        
        public function getTechData($id){
            
            $bdd = parent::getBdd();
            $req = $bdd->prepare("SELECT id, nom, prenom FROM technicien WHERE id = :id");
            $req->bindValue(':id', $id);
            $req->execute();
            $tech_data = $req->fetchAll(PDO::FETCH_ASSOC);
            return $tech_data;
        }
        
        
        public function saveIntervention($intervention){
              
            if($intervention['date_intervention'] != ""){
                $date = $intervention['date_intervention'];
            }else{
                $date = date('Y-m-d H:i:s');
            }
            
            $id_tech = $intervention['id_tech'];
            $intervention = array_diff($intervention,array('0') );
            unset($intervention['id_tech']);
            unset($intervention['saveIntervention']);
            unset($intervention['date_intervention']);
            
            foreach($intervention as $key => $value){
            
              $type_intervention = $key;
              $nb_intervention = $value; 
              $bdd = $this->getBdd();
              $insert = $bdd->prepare("INSERT INTO intervention (date_intervention, type_intervention, nb_intervention, id_technicien) VALUES (:date_intervention, :type_intervention, :nb_intervention, :id_technicien)");
              $insert->bindParam(':date_intervention', $date);
              $insert->bindParam(':type_intervention', $type_intervention);
              $insert->bindParam(':nb_intervention', $nb_intervention);
              $insert->bindParam(':id_technicien', $id_tech);
              $insert->execute();
                }
                
                return true;
        
        }
}