<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ajaxModel
 *
 * @author emman
 */
class ajaxModel extends Model{
    //put your code here
    
    protected $bdd;
    
    public function __construct() {
        
         $this->bdd = parent::getBdd();
    }
    
    public function checkdate($date) {
        
        $req = $this->bdd->prepare("SELECT * FROM intervention WHERE date_intervention = :date_inter");
        $req->bindValue(':date_inter', $date);
        $req->execute();
        if($req->rowCount() > 0){
            return true;
        }else{
            return false;
        }
    }
    
    public function checkdateHours($date) {
        
        $req = $this->bdd->prepare("SELECT * FROM horaires WHERE  date = :date_inter");
        $req->bindValue(':date_inter', $date);
        $req->execute();
        if($req->rowCount() > 0){
            return true;
        }else{
            return false;
        }
    }
    
    public function get_tech_date_hours($date){
        $req = $this->bdd->prepare("SELECT * FROM horaires WHERE  date = :date_inter");
        $req->bindValue(':date_inter', $date);
        $tech_data = $req->fetchAll(PDO::FETCH_ASSOC);
        return $tech_data;
    }
    
    
    
    public function getTechData( ){
        
            $req = $this->bdd->prepare("SELECT * FROM technicien");
            $req->execute();
            $tech_data = $req->fetchAll(PDO::FETCH_ASSOC);
            return $tech_data;
            
    }
    
 
    public function insertInterventions($interventions,$dayInter) {
        
         //var_dump($interventions);
         
         foreach($interventions as $value){
            
             $tech_id = $value['techid'];
             $type_inter = $value['name'];
             $nb_inter = $value['value'];
                   
             //insertion bdd 
             $req = $this->bdd->prepare("INSERT INTO `intervention`(date_intervention, type_intervention, nb_intervention, id_technicien) VALUES (:day_inter, :type_inter, :nb_inter, :tech_id)");
             $req->bindValue(':day_inter', $dayInter);
             $req->bindValue(':tech_id', $tech_id);
             $req->bindValue(':nb_inter', $nb_inter);
             $req->bindValue(':type_inter', $type_inter);

             if($req->execute()){
                 
                 // if nb_inter > 1 on doit 
                    if($type_inter === "vol_confirme"){
                        
                        
                        $arr_size = intval($nb_inter); 
                        $empty_arr = array_fill(0, $arr_size,NULL);
                        
                        foreach($empty_arr as $inter){
                            
                            $req = $this->bdd->prepare('SELECT max(id) as max_id from intervention');
                            if($req->execute()){
                                $last_id = $req->fetch(PDO::FETCH_ASSOC);
                                $this->insertVolConfirme($value, $dayInter, $last_id);
                            }
                            
                        }
                 
                    
                    }
             }
             
                   
         }
         
    }
    
    
    public function insertVolConfirme($data_vol, $dayInter, $last_id) {
        
            $req = $this->bdd->prepare("INSERT INTO `vol_confirme`(date, client, adresse, informations, id_tech, id_intervention) VALUES (:day_inter, :client, :adresse, :informations, :tech_id, :id_intervention)");
             $req->bindValue(':day_inter', $dayInter);
             $req->bindValue(':client', "a renseigner");
             $req->bindValue(':adresse', 'a renseigner');
             $req->bindValue(':informations', 'a renseigner');
             $req->bindValue(':tech_id', $data_vol['techid']);
             $req->bindValue(':id_intervention', $last_id['max_id']);
             $req->execute();
        
    }
    
    
     public function updateInterventions($interventions,$dayInter) {
       
         // récupérer les id des interventions à update

         foreach($interventions as $inter){
             
             $tech_id = $inter['techid'];
             $type_inter = $inter['name'];
             $nb_inter = $inter['value'];
             
             //var_dump($dayInter); 
             
             $req = $this->bdd->prepare("SELECT * FROM intervention WHERE date_intervention = :day_inter AND id_technicien = :tech_id AND type_intervention = :type_inter");
             $req->bindValue(':day_inter', $dayInter);
             $req->bindValue(':tech_id', $tech_id);
             $req->bindValue(':type_inter', $type_inter);
             $req->execute();
             if($req->rowCount() > 0){
                 
                // pour cette date et ce tech, nous avons déjà des informations.
                $result = $req->fetchAll(PDO::FETCH_ASSOC);
 
                foreach($result as $key => $value){
                   
                    
                    var_dump($value);
                    
                    $id_inter_to_update = $value['id'];
                    $nb_intervention = $value['nb_intervention'];
                    
                    // si nb inter vaut zero, alors on delete la ligne
                    if($nb_inter == 0){
                        
                        $delete = $this->bdd->prepare('DELETE FROM `intervention` WHERE id = :id');
                        $delete->bindParam(':id', $id_inter_to_update);
                        $delete->execute();
                    }else {

                        
                        $update = $this->bdd->prepare("UPDATE intervention SET nb_intervention = :nb_intervention WHERE id = :id");
                        $update->bindValue(':nb_intervention',$nb_inter);
                        $update->bindValue(':id',$id_inter_to_update);
                       
                        $resp = $update->execute();
                        
                      
                    }
                    
                }
                
            
                
             }else{
                 
                 // si val interventon différent de 0, on insère .
                 
                 if($nb_inter != 0){
                     
                    $req = $this->bdd->prepare("INSERT INTO `intervention`(date_intervention, type_intervention, nb_intervention, id_technicien) VALUES (:day_inter, :type_inter, :nb_inter, :tech_id)");
                    $req->bindValue(':day_inter', $dayInter);
                    $req->bindValue(':tech_id', $tech_id);
                    $req->bindValue(':nb_inter', $nb_inter);
                    $req->bindValue(':type_inter', $type_inter);
                    $req->execute();
                    
                 
             }
         }
   
    }
      }
    
    public function getDayData($day) {
        
        $req = $this->bdd->prepare("SELECT * FROM intervention WHERE date_intervention = :date_inter");
        $req->bindValue(':date_inter', $day);
        $req->execute();
        $dayData = $req->fetchAll(PDO::FETCH_ASSOC);
        return $dayData;
    }
    
    public function get_plage_interventions($day_start, $day_end){
        
        
        $req = $this->bdd->prepare("SELECT * FROM intervention WHERE date_intervention BETWEEN :day_start and :day_end");
        $req->bindValue(':day_start', $day_start);
        $req->bindValue(':day_end', $day_end);
        $req->execute();
        $inter_btw_date = $req->fetchAll(PDO::FETCH_ASSOC);
        
        return $inter_btw_date;
    }
    
    
    public function get_type_interventions(){
        
        $req = $this->bdd->prepare("SELECT * FROM type_intervention");
        $req->execute();
        $types_inter = $req->fetchAll();
        
        return $types_inter;
    }
    
    
    public function get_techniciens(){
        
        $req = $this->bdd->prepare("SELECT * FROM technicien");
        $req->execute();
        $techniciens = $req->fetchAll(PDO::FETCH_ASSOC);
        
        return $techniciens;
    }
    
    public function get_prestas(){
        
        $req = $this->bdd->prepare("SELECT * FROM prestataire");
        $req->execute();
        $prestas = $req->fetchAll(PDO::FETCH_ASSOC);
        return $prestas;
    }
    
    
    
    public function get_cumul_interventions($dayStart, $dayEnd){
       
       
        $cumul_interventions = array( );
        $types_inter = $this->get_type_interventions( );
        $techniciens = $this->get_techniciens( );
        
        // pour chaque type intervention
        foreach($types_inter as $key_type => $type) {
            
            // pour chaque tech
            foreach($techniciens as $key_tech => $technicien) {
              
                $req = $this->bdd->prepare("SELECT id_technicien, type_intervention, SUM(nb_intervention) as nb_intervention from intervention WHERE id_technicien = :id_tech and type_intervention = :type_intervention and date_intervention BETWEEN :day_start and :day_end");
                $req->bindValue(':day_start', $dayStart);
                $req->bindValue(':day_end', $dayEnd);
                $req->bindValue(':type_intervention', $type['type']);
                $req->bindValue(':id_tech', $technicien['id']);
                $req->execute();
                
                if($req->rowCount() > 0){
                    
                    $result = $req->fetch(PDO::FETCH_ASSOC);
                    
                    if(!empty($result) && $result['id_technicien'] != NULL){
                     array_push($cumul_interventions, $result);
                    }
                
                }
                    
            }
        }
            return $cumul_interventions;
    }
    
    
    public function add_presta($presta_name, $presta_msg) {
        
        if($presta_name != ""){
            
             $req = $this->bdd->prepare("INSERT INTO `prestataire`(nom, message) VALUES (:presta_name, :presta_msg)");
             $req->bindValue(':presta_name', $presta_name);
             $req->bindValue(':presta_msg', $presta_msg);
             $resp = $req->execute();
             
             var_dump($resp);
             $date = date('Y-m-d');
             // on insérer le prestataire comme un tech avec statut prestataire
             $req = $this->bdd->prepare("INSERT INTO `technicien`(date_creation, nom, prenom, statut) VALUES (:date_creation, :nom, :prenom, :statut)");
             $req->bindValue(':date_creation', $date);
             $req->bindValue(':nom', $presta_name);
             $req->bindValue(':prenom', "prestataire");
             $req->bindValue(':statut', "prestataire");
             $req->execute();
             
             return $resp;
             
        }
        
        return FALSE;
             
         
    }
    
    
    public function add_tech($tech_data) {
        
             $req = $this->bdd->prepare("INSERT INTO `technicien`(date_creation, nom, prenom, statut) VALUES (:date_creation, :nom, :prenom, :statut)");
             $req->bindValue(':date_creation', $tech_data['date_creation']);
             $req->bindValue(':nom', $tech_data['name']);
             $req->bindValue(':prenom', $tech_data['prename']);
             $req->bindValue(':statut', $tech_data['statut']);
             $resp = $req->execute();
             
             var_dump($resp);
             return $resp;
    }
    
    
    public function get_vols(){
        
        $req = $this->bdd->prepare("SELECT * FROM vol_confirme");
        $req->execute();
        $techniciens = $req->fetchAll(PDO::FETCH_ASSOC);
        
        return $techniciens;
    }
    
    
    public function update_vol($vol){
        
      
        $req = $this->bdd->prepare("UPDATE vol_confirme SET client = :client, adresse = :adresse, informations = :informations WHERE id = :id");
        $req->bindValue(':client', $vol['volnomClient']);
        $req->bindValue(':adresse',$vol['volAdresse']);
        $req->bindValue(':informations',$vol['volMsg'] );
        $req->bindValue(':id',$vol['idVol']);
        $req->execute();
        
    }
    
    
    public function insert_horaires($date, $is_ferie, $isDimanche, $horaires){
        
        
        if($is_ferie === "true"){
            $is_ferie = 1;
        }else{
            $is_ferie = 0;
        }
        
        var_dump($isDimanche);
        
          if($isDimanche === "true"){
            $isDimanche = 1;
        }else{
            $isDimanche = 0;
        }
        
        
        
        foreach($horaires as $key => $value){
            
            $tech_id = $key; 
            $req = $this->bdd->prepare("INSERT INTO `horaires`(date, debut_matin, fin_matin, debut_aprem, fin, is_ferie, idtech, is_dimanche) VALUES (:date, :debut_matin, :fin_matin, :debut_aprem, :fin, :is_ferie, :idtech, :is_dimanche)");
            $req->bindValue(':date', $date);
            $req->bindValue(':debut_matin',$value['debut_matin']);
            $req->bindValue(':fin_matin',$value['fin_matin']);
            $req->bindValue(':debut_aprem',$value['debut_aprem']);
            $req->bindValue(':fin',$value['fin']);
            $req->bindValue(':is_ferie',$is_ferie);
            $req->bindValue(':idtech',$tech_id);
            $req->bindValue(':is_dimanche',$isDimanche);
            $req->execute();
            
        }        
      
        }
      
        
        
    
   
}
