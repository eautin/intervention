<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoginModel
 *
 * @author emman
 */
class LoginModel extends Model {
    
    public function __construct() {
       
    }
    
    public function validateLogin($user,$pass) {
        $bdd = parent::getBdd();
        $req = $bdd->prepare("SELECT * FROM user WHERE login = :login AND password = :password");
        $req->bindValue(':login', $user);
        $req->bindValue(':password', $pass);
        $req->execute();
        if($req->rowCount() > 0){
            return true;
        }else{
            return false;
        }
    } 
    
    
    public function getRole($user) {
        
        $bdd = parent::getBdd();
        $req = $bdd->prepare("SELECT role FROM user WHERE login = :login");
        $req->bindValue(':login', $user);
        $req->execute();
        $role = $req->fetch(PDO::FETCH_OBJ);
        return $role->role;
    }
    
}
